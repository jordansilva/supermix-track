package br.com.supermix.track;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.supermix.track.domain.Trip;
import br.com.supermix.track.dummy.DummyContent.DummyItem;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link TripHistoryFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyTripHistoryRecyclerViewAdapter extends RecyclerView.Adapter<MyTripHistoryRecyclerViewAdapter.ViewHolder> {

    private final List<Trip> mValues;
    private final TripHistoryFragment.OnListFragmentInteractionListener mListener;
    private final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    private final SimpleDateFormat sdfHour = new SimpleDateFormat("HH:mm", Locale.getDefault());

    public MyTripHistoryRecyclerViewAdapter(ArrayList<Trip> items, TripHistoryFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_triphistory, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        String name = String.format("%s (VIAGEM %d)", mValues.get(position).destination.name,
                position+1);
        holder.mDestinationName.setText(name);
        holder.mDestinationAddress.setText(mValues.get(position).destination.address);
        //Trip
        //holder.mDateTrip.setText(sdfDate.format(mValues.get(position).startTrip));
        holder.mStartTrip.setText(sdfHour.format(mValues.get(position).startTrip));
        holder.mEndTrip.setText(sdfHour.format(mValues.get(position).endTrip));
        //Land
        if (mValues.get(position).startLandTime != null)
        holder.mStartLand.setText(sdfHour.format(mValues.get(position).startLandTime));

        if (mValues.get(position).endLandTime != null)
            holder.mEndLand.setText(sdfHour.format(mValues.get(position).endLandTime));
        //Departure
        //TODO: Need change and implement endLandTime
        holder.mArrival.setText(sdfHour.format(mValues.get(position).arrivalTime));
        holder.mDeparture.setText(sdfHour.format(mValues.get(position).departureTime));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mDestinationName;
        public final TextView mDestinationAddress;
        //Trip
        //public final TextView mDateTrip;
        public final TextView mStartTrip;
        public final TextView mEndTrip;
        //Land
        public final TextView mStartLand;
        public final TextView mEndLand;
        //Departure and Arrival
        public final TextView mArrival;
        public final TextView mDeparture;

        public Trip mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDestinationName = (TextView) view.findViewById(R.id.trip_name);
            mDestinationAddress = (TextView) view.findViewById(R.id.trip_address);
            //mDateTrip = (TextView) view.findViewById(R.id.trip_date);
            //Trip
            mStartTrip = (TextView) view.findViewById(R.id.trip_start);
            mEndTrip = (TextView) view.findViewById(R.id.trip_end);
            //Landing
            mStartLand = (TextView) view.findViewById(R.id.trip_start_land);
            mEndLand = (TextView) view.findViewById(R.id.trip_stop_land);
            //Departure and Arrival
            mArrival = (TextView) view.findViewById(R.id.trip_arrival);
            mDeparture = (TextView) view.findViewById(R.id.trip_departure);

        }
    }
}
