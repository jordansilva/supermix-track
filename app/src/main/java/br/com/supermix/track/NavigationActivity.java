package br.com.supermix.track;

import android.app.Fragment;
import android.app.FragmentManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import br.com.supermix.track.helper.AlarmUtil;
import br.com.supermix.track.ui.listener.OnFragmentInteractionListener;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnFragmentInteractionListener {

    ActionBarDrawerToggle mToggle;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.nav_view)
    NavigationView mNavigationView;
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    Fragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);

        configureUi();
        init();
    }

    private void configureUi() {
        //ActionBar
        setSupportActionBar(mToolbar);

        //Navigation Drawer
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mNavigationView.setNavigationItemSelectedListener(this);
        mDrawerLayout.setDrawerListener(mToggle);
        mToggle.syncState();

        onNavigationItemSelected(mNavigationView.getMenu().getItem(0));
    }

    private void init() {
        configureAlarms();
    }

    protected void configureAlarms() {
//        AlarmUtil util = new AlarmUtil(getApplicationContext());
//        util.createAlarmTracking();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment = null;

        if (item == null)
            return false;

        int id = item.getItemId();

        switch (id) {
            case R.id.nav_trips:
                fragment = TripsFragment.newInstance();
                break;
            case R.id.nav_history:
                fragment = TripHistoryFragment.newInstance();
                break;
            case R.id.nav_settings:
                break;
        }

        if (fragment != null && (mFragment == null || mFragment.getClass() != fragment.getClass())) {
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.main_content, fragment).commit();

            mFragment = fragment;
            item.setChecked(true);
            mDrawerLayout.closeDrawer(GravityCompat.START);

            return true;
        } else {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return false;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
