package br.com.supermix.track;

import android.Manifest;
import android.app.Fragment;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import javax.inject.Inject;

import br.com.supermix.track.domain.Trip;
import br.com.supermix.track.helper.Constants;
import br.com.supermix.track.service.PlaceService;
import br.com.supermix.track.ui.listener.OnFragmentInteractionListener;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TripsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TripsFragment extends Fragment implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String INTENT_NAME = "br.com.supermix.track.ProximityReceiver";
    private static final int MY_LOCATION_REQUEST_CODE = 100;
    private static final int LOCATION_REQUEST_CODE = 200;
    @Inject
    PlaceService service;
    @Inject
    LocationManager mLocationManager;
    @Bind(R.id.no_delivery)
    LinearLayout mNoDelivery;
    @Bind(R.id.trips_rel_layout)
    CoordinatorLayout mRelativeLayout;
    @Bind(R.id.rel_trip)
    RelativeLayout mRelativeTripDescription;
    @Bind(R.id.txt_destination)
    TextView mDestination;
    @Bind(R.id.txt_prev_destination)
    TextView mArrivalEstimateDestination;
    @Bind(R.id.table_ok_destination)
    TableRow mTableArrivalTimeDestination;
    @Bind(R.id.txt_ok_destination)
    TextView mArrivalTimeDestination;
    @Bind(R.id.fab)
    FloatingActionButton mFloatActionButton;
    @Bind(R.id.background_progress)
    LinearLayout mBackgroundProgress;
    @Bind(R.id.progress_wheel)
    ProgressWheel mProgressBar;
    @Bind(R.id.map)
    MapView mapView;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mGoogleMap;
    private ArrayList<Trip> mTrips;
    private Trip mTrip;
    private ProximityReceiver mReceiver = new ProximityReceiver();

    public TripsFragment() {
    }

    public static TripsFragment newInstance() {
        return new TripsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_trips, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_trips, container, false);
        ButterKnife.bind(this, view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        service = new PlaceService(getActivity());
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        configureUi();
        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        if (item.getItemId() == R.id.over_simulate) {
            UUID gibran = UUID.fromString("917f67ef-750d-48ff-a1d0-db67b4dc6d41");
            UUID boiZe = UUID.fromString("b1b14350-e7a9-4e18-a8ac-e837f2eaa9dd");
            simulateTrip(gibran, boiZe);
        }
        if (item.getItemId() == R.id.over_simulate2) {
            UUID jordan = UUID.fromString("0f00371f-9314-4332-8228-fa63d9c01256");
            UUID superMix = UUID.fromString("f0929300-4132-455f-9fb6-0e943501b34f");
            simulateTrip(superMix, jordan);
        }
        return true;
    }

    protected void configureUi() {
        //Check Trips Exists
        mTrips = new PlaceService(getActivity()).listTrips();
        if (mTrips != null && mTrips.size() > 0) {
            mNoDelivery.setVisibility(View.GONE);
            mRelativeTripDescription.setVisibility(View.VISIBLE);
            mRelativeLayout.setBackgroundResource(android.R.color.background_light);

            mTrip = mTrips.get(0);
            mDestination.setText(mTrip.destination.address);

            //Arrival Estimative
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.getDefault());
            mArrivalEstimateDestination.setText(sdf.format(mTrip.estimateArrivalTime));

            //Arrival Time Destination
            if (mTrip.arrivalTime != null) {
                mTableArrivalTimeDestination.setVisibility(View.VISIBLE);
                mArrivalTimeDestination.setText(sdf.format(mTrip.arrivalTime));
            } else {
                mTableArrivalTimeDestination.setVisibility(View.GONE);
                mArrivalTimeDestination.setText("");
            }
        } else {
            mNoDelivery.setVisibility(View.VISIBLE);
            mRelativeTripDescription.setVisibility(View.GONE);
            mRelativeLayout.setBackgroundResource(R.color.colorSecondaryText);
        }
    }

    protected void init() {
        if (mTrips != null && mTrips.size() > 0) {
            Trip trip = mTrips.get(0);

            addAlert(trip.id, trip.warehouse.id, "WAREHOUSE", trip.warehouse.latitude,
                    trip.warehouse.longitude, 1000);
            addAlert(trip.id, trip.destination.id, "DESTINATION", trip.destination.latitude,
                    trip.destination.longitude, 1001);
        }
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(Constants.LOG.RADIUS_DISTANCE);
        return mLocationRequest;
    }

    protected void startLocationUpdates() {
        int hasFineLocation = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        int hasCoarseLocation = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        if (hasFineLocation != PackageManager.PERMISSION_GRANTED && hasCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_CODE);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, createLocationRequest(), this);
        }
    }

    protected void addAlert(UUID id, UUID place_id, String place, double latitude, double longitude, int requestCode) {
        int hasFineLocation = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        int hasCoarseLocation = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        if (hasFineLocation != PackageManager.PERMISSION_GRANTED && hasCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, MY_LOCATION_REQUEST_CODE);
            return;
        }

        Intent intent = new Intent(INTENT_NAME);
        intent.putExtra("ID", id.toString());
        intent.putExtra("PLACE_ID", place_id.toString());
        intent.putExtra(place, true);
        intent.putExtra("LATITUDE", latitude);
        intent.putExtra("LONGITUDE", longitude);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), requestCode, intent, PendingIntent.FLAG_ONE_SHOT);

        mLocationManager.addProximityAlert(latitude, longitude, Constants.LOG.RADIUS_DISTANCE, -1, pendingIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_LOCATION_REQUEST_CODE || requestCode == LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED)
                    mGoogleMap.setMyLocationEnabled(true);
                if (requestCode == MY_LOCATION_REQUEST_CODE)
                    init();
                if (requestCode == LOCATION_REQUEST_CODE)
                    startLocationUpdates();
            }
        }
    }

    private void simulateTrip(UUID from, UUID to) {
        String result = "Viagem criada!";
        if (!service.simulate(from, to))
            result = "Erro de simulação!";
        else
            reloadTrip();

        Snackbar.make(getView(), result, Snackbar.LENGTH_SHORT).show();
    }

    protected void reloadTrip() {
        configureUi();
        init();
        traceTrip();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            IntentFilter filter = new IntentFilter(INTENT_NAME);
            getActivity().registerReceiver(mReceiver, filter);

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        mapView.onLowMemory();
        super.onLowMemory();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.setBuildingsEnabled(true);
        mGoogleMap.setTrafficEnabled(true);
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(true);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        traceTrip();

        int hasFineLocation = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        int hasCoarseLocation = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        if (hasFineLocation != PackageManager.PERMISSION_GRANTED && hasCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, MY_LOCATION_REQUEST_CODE);
        } else {
            mGoogleMap.setMyLocationEnabled(true);
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        moveCamera(location.getLatitude(), location.getLongitude(), 16.f);
        updateTrip(location);
    }

    private void moveCamera(double latitude, double longitude, float zoomDistance) {
        LatLng latLng = new LatLng(latitude, longitude);

        CameraPosition.Builder cameraPosition = new CameraPosition.Builder();
        cameraPosition.target(latLng);
        cameraPosition.zoom(zoomDistance);
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition.build()));
    }

    protected void traceTrip() {
        if (mTrip != null) {
            MarkerOptions initial = new MarkerOptions()
                    .position(new LatLng(mTrip.warehouse.latitude, mTrip.warehouse.longitude))
                    .title(mTrip.warehouse.name)
                    .icon(BitmapDescriptorFactory.defaultMarker());

            MarkerOptions destination = new MarkerOptions()
                    .position(new LatLng(mTrip.destination.latitude, mTrip.destination.longitude))
                    .title(mTrip.destination.address)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));


            mGoogleMap.addMarker(initial);
            mGoogleMap.addMarker(destination);
            moveCamera(mTrip.warehouse.latitude, mTrip.warehouse.longitude, 16.f);
        }
    }

    protected void removeMarkers() {
        mGoogleMap.clear();
    }

    protected void updateTrip(Location location) {
        if (mTrip != null && (mTrip.startTrip == null || mTrip.endTrip == null)) {

            Location warehouse = new Location("Base");
            warehouse.setLatitude(mTrip.warehouse.latitude);
            warehouse.setLongitude(mTrip.warehouse.longitude);

            Location destination = new Location("Base");
            destination.setLatitude(mTrip.destination.latitude);
            destination.setLongitude(mTrip.destination.longitude);

            //Leaves warehouse by distance
            if (mTrip.startTrip == null && location.distanceTo(warehouse) > Constants.LOG.RADIUS_DISTANCE)
                updateTrip(true, false, false);

            //Entering WareHouse by Distance
            if (location.distanceTo(warehouse) <= Constants.LOG.RADIUS_DISTANCE)
                updateTrip(true, false, true);

            //Entering LandMark
            if (mTrip.startTrip != null &&
                    location.distanceTo(destination) <= Constants.LOG.RADIUS_DISTANCE)
                updateTrip(false, true, true);

            //Leaving LandMark
            if (mTrip.arrivalTime != null && location.distanceTo(destination) > Constants.LOG.RADIUS_DISTANCE)
                updateTrip(false, true, false);

        }
    }

    protected void updateTrip(boolean isWareHouse, boolean isDestination, boolean isEntering) {
        if (mTrip != null && (mTrip.startTrip == null || mTrip.endTrip == null)) {
            Date time = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.getDefault()).getTime();

            //Leaves warehouse
            if (isWareHouse && !isEntering && mTrip.startTrip == null) {
                mTrip.startTrip = time;
            }

            //Arrival on destination
            if (isDestination && isEntering && mTrip.startTrip != null && mTrip.arrivalTime == null) {
                mTrip.arrivalTime = time;
            }

            if (isDestination && isEntering && mBackgroundProgress.getVisibility() == View.GONE) {
                showFabButton();
            }

            //Leaves destination (back to warehouse?)
            if (isDestination && !isEntering && mTrip.startTrip != null) {
                hideFabButton();
                mTrip.departureTime = time;
            }

            //End of travel
            if (isWareHouse && isEntering && mTrip.startTrip != null &&
                    mTrip.departureTime != null && mTrip.endTrip == null) {
                mTrip.endTrip = time;
                removeMarkers();
            }

            saveTrip();
        }
    }

    protected void saveTrip() {
        service.saveTrip(mTrip);

        if (mTrip.endTrip != null) {
            AlertDialog.Builder builder =
                    new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.travel);
            builder.setMessage(R.string.travel_successful);
            builder.setNeutralButton("OK", null);
            builder.show();
        }

        configureUi();
    }

    protected void showFabButton() {
        mFloatActionButton.show();
    }

    protected void hideFabButton() {
        mFloatActionButton.hide();
    }

    @OnClick(R.id.fab)
    public void onFloatingButtonClick(View view) {
        mTrip.startLandTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"),
                Locale.getDefault()).getTime();
        saveTrip();

        hideFabButton();

        mBackgroundProgress.setVisibility(View.VISIBLE);
        mProgressBar.spin();

        Snackbar.make(getView(), R.string.landing, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.stop_land, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showFabButton();
                        mBackgroundProgress.setVisibility(View.GONE);
                        mProgressBar.stopSpinning();

                        mTrip.endLandTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"),
                                Locale.getDefault()).getTime();
                        saveTrip();
                    }
                }).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public class ProximityReceiver extends BroadcastReceiver {
        public ProximityReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            // Key for determining whether user is leaving or entering
            String key = LocationManager.KEY_PROXIMITY_ENTERING;

            boolean state = intent.getBooleanExtra(key, false);
            boolean warehouse = intent.getBooleanExtra("WAREHOUSE", false);
            boolean destination = intent.getBooleanExtra("DESTINATION", false);

            updateTrip(warehouse, destination, state);
        }
    }


}
