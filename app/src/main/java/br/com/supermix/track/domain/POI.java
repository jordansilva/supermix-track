package br.com.supermix.track.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.UUID;

/**
 * Created by jordansilva on 2/7/16.
 */
@DatabaseTable(tableName = "poi")
public class POI extends DomainBase {

    @DatabaseField(allowGeneratedIdInsert = true, generatedId = true)
    public UUID id;
    @DatabaseField
    public String name;
    @DatabaseField
    public String description;
    @DatabaseField
    public String address;
    @DatabaseField
    public String number;
    @DatabaseField
    public String city;
    @DatabaseField
    public String state;
    @DatabaseField
    public double latitude;
    @DatabaseField
    public double longitude;

    public POI() {

    }

    public POI(UUID id, String name, String description, String address, String number,
               String city, String state, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.address = address;
        this.number = number;
        this.city = city;
        this.state = state;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public POI(Parcel in) {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Parcelable.Creator<POI> CREATOR
            = new Parcelable.Creator<POI>() {
        public POI createFromParcel(Parcel in) {
            return new POI(in);
        }

        public POI[] newArray(int size) {
            return new POI[size];
        }
    };
}
