package br.com.supermix.track.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;
import java.util.UUID;

/**
 * Created by jordansilva on 2/10/16.
 */
@DatabaseTable(tableName = "route")
public class Route extends DomainBase {

    @DatabaseField(generatedId = true)
    public UUID id;
    @DatabaseField(foreignAutoRefresh = false, foreign = true)
    public Trip trip;
    @DatabaseField
    public double latitude;
    @DatabaseField
    public double longitude;
    @DatabaseField
    public Date date;
    @DatabaseField
    public float accuracy;
    @DatabaseField
    public String provider;
    @DatabaseField
    public float batteryLevel;

    public Route() {
    }

    public Route(Parcel in) {

    }

    public Route(UUID id, Trip trip, double latitude, double longitude, Date date) {
        this.id = id;
        this.trip = trip;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Parcelable.Creator<Route> CREATOR
            = new Parcelable.Creator<Route>() {
        public Route createFromParcel(Parcel in) {
            return new Route(in);
        }

        public Route[] newArray(int size) {
            return new Route[size];
        }
    };
}
