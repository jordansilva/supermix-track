package br.com.supermix.track.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;
import java.util.UUID;

/**
 * Created by jordansilva on 2/7/16.
 */
@DatabaseTable(tableName = "trip")
public class Trip extends DomainBase {

    public static final Parcelable.Creator<Trip> CREATOR
            = new Parcelable.Creator<Trip>() {
        public Trip createFromParcel(Parcel in) {
            return new Trip(in);
        }

        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };
    @DatabaseField(generatedId = true)
    public UUID id;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            canBeNull = false)
    public POI warehouse;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true,
            canBeNull = false)
    public POI destination;
    @DatabaseField
    public Date startTrip;
    @DatabaseField
    public Date endTrip;
    @DatabaseField
    public Date arrivalTime;
    @DatabaseField
    public Date departureTime;
    @DatabaseField
    public Date estimateArrivalTime;
    @DatabaseField
    public Date estimateStartLandTime;
    @DatabaseField
    public Date startLandTime;
    @DatabaseField
    public Date endLandTime;

    public Trip() {

    }

    public Trip(POI warehouse, POI destination) {
        this.warehouse = warehouse;
        this.destination = destination;
    }

    public Trip(UUID id, POI warehouse, POI destination, Date startTrip, Date endTrip,
                Date startLandTime, Date endLandTime) {
        this.id = id;
        this.warehouse = warehouse;
        this.destination = destination;
        this.startTrip = startTrip;
        this.endTrip = endTrip;
        this.startLandTime = startLandTime;
        this.endLandTime = endLandTime;
    }

    public Trip(Parcel in) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
