package br.com.supermix.track.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/**
 * Created by jordansilva on 2/10/16.
 */
public class AlarmUtil {

    private Context mContext;
    private AlarmManager mAlarmManager;

    public AlarmUtil(Context context) {
        mContext = context;
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public void createAlarmTracking() {
        Intent intent = new Intent(mContext, ServiceTrack.class);
        PendingIntent pendingIntent = PendingIntent.getService(mContext, 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()
                + (60 * 1000), Constants.LOG.INTERVAL, pendingIntent);
    }
}
