package br.com.supermix.track.helper;

/**
 * Created by jordansilva on 2/10/16.
 */
public class Constants {

    public class LOG {
        public static final int INTERVAL = 60000;
        public static final int INTERVAL_LOG_PLACE = 300000;
        public static final float RADIUS_DISTANCE = 100.f;
    }
}
