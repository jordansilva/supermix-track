package br.com.supermix.track.helper;

import android.Manifest;
import android.app.AlarmManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;

import static android.location.LocationManager.GPS_PROVIDER;
import static android.location.LocationManager.NETWORK_PROVIDER;
import static android.location.LocationManager.PASSIVE_PROVIDER;

/**
 * Created by jordansilva on 2/10/16.
 */
public class LocationManagerOffline {


    protected static LocationManager mLocationManager;

    private static final long INTERVAL = AlarmManager.INTERVAL_HOUR;
    private static Location mLocation;
    private static ArrayList<String> mListSearching;
    private Context mContext;
    private LocationManagerOfflineListener mListener;

    public LocationManagerOffline(Context context) {
        mContext = context;
        mListSearching = new ArrayList<String>();
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        requestLocation();
    }

    /*
        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            switch (requestCode) {
                case REQUEST_CODE_ASK_PERMISSIONS:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission Granted
                        insertDummyContact();
                    } else {
                        // Permission Denied
                        Toast.makeText(MainActivity.this, "WRITE_CONTACTS Denied", Toast.LENGTH_SHORT)
                                .show();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
     */

    public Location getLastLocationKown() {
        if (mLocation == null)
            requestLocation();

        return mLocation;
    }

    public void requestLocation() {

        int hasFineLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
        int hasCoarseLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (hasFineLocation != PackageManager.PERMISSION_GRANTED && hasCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        boolean isGpsEnable = mLocationManager.isProviderEnabled(GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(NETWORK_PROVIDER);
        boolean isPassiveProviderEnabled = mLocationManager.isProviderEnabled(PASSIVE_PROVIDER);

        if (isNetworkEnabled && !mListSearching.contains(NETWORK_PROVIDER)) {
            mListSearching.add(NETWORK_PROVIDER);
            mLocationManager.requestLocationUpdates(NETWORK_PROVIDER, INTERVAL, 0,
                    mLocationListenerNetwork);

            if (mLocationManager != null) {
                Location location = mLocationManager.getLastKnownLocation(NETWORK_PROVIDER);
                if (location != null)
                    mLocation = location;
            }
        }

        if (isGpsEnable && !mListSearching.contains(GPS_PROVIDER)) {
            mListSearching.add(GPS_PROVIDER);
            mLocationManager.requestLocationUpdates(GPS_PROVIDER, INTERVAL, 0, mLocationListenerGPS);

            if (mLocationManager != null) {
                Location location = mLocationManager.getLastKnownLocation(GPS_PROVIDER);
                if (location != null)
                    mLocation = location;
            }
        }

        if (isPassiveProviderEnabled && !mListSearching.contains(PASSIVE_PROVIDER)) {
            mListSearching.add(PASSIVE_PROVIDER);
            mLocationManager.requestLocationUpdates(PASSIVE_PROVIDER, INTERVAL, 0,
                    mLocationListenerPassive);

            if (mLocationManager != null) {
                Location location = mLocationManager.getLastKnownLocation(PASSIVE_PROVIDER);
                if (location != null)
                    mLocation = location;
            }
        }

    }

    public void setListener(LocationManagerOfflineListener listener) {
        mListener = listener;
    }

    public void stop() {
        if (mListSearching.isEmpty()) {
            int hasFineLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
            int hasCoarseLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (hasFineLocation != PackageManager.PERMISSION_GRANTED && hasCoarseLocation != PackageManager.PERMISSION_GRANTED)
                return;

            mLocationManager.removeUpdates(mLocationListenerGPS);
            mLocationManager.removeUpdates(mLocationListenerNetwork);
            mLocationManager.removeUpdates(mLocationListenerPassive);
        }
    }

    private Criteria getCriteria() {
        Criteria crta = new Criteria();
        crta.setAccuracy(Criteria.ACCURACY_FINE);
        crta.setAltitudeRequired(false);
        crta.setBearingRequired(false);
        crta.setCostAllowed(false);
        crta.setPowerRequirement(Criteria.POWER_LOW);

        return crta;
    }

    private void setLocation(Location location) {
        if (isBetterLocation(location, mLocation)) {
            mLocation = location;
            if (mListener != null)
                mListener.onLocationChanged(location);
        }
    }

    /**
     * Determines whether one Location reading is better than the current
     * Location fix
     *
     * @param location
     *            The new Location that you want to evaluate
     * @param currentBestLocation
     *            The current Location fix, to which you want to compare the new
     *            one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null)
            return true;

        if (location == null)
            return false;

        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > INTERVAL;
        boolean isSignificantlyOlder = timeDelta < -INTERVAL;
        boolean isNewer = timeDelta > 0;

        if (isSignificantlyNewer) {
            return true;
        } else if (isSignificantlyOlder) {
            return false;
        }

        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and
        // accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private LocationListener mLocationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            setLocation(location);
            // mListSearching.remove(location.getProvider());
            // stop();
        }

        @Override
        public void onProviderDisabled(String provider) {
            // mListSearching.remove(provider);
            // stop();
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };

    private LocationListener mLocationListenerNetwork = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            setLocation(location);
            // mListSearching.remove(location.getProvider());
            // stop();
        }

        @Override
        public void onProviderDisabled(String provider) {
            // mListSearching.remove(provider);
            // stop();
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };

    private LocationListener mLocationListenerPassive = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            setLocation(location);
            // mListSearching.remove(location.getProvider());
            // stop();
        }

        @Override
        public void onProviderDisabled(String provider) {
            // mListSearching.remove(provider);
            // stop();
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }
    };

    public static interface LocationManagerOfflineListener {
        void onLocationChanged(Location location);
    }

}
