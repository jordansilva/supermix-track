package br.com.supermix.track.helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by jordansilva on 2/10/16.
 */
public class LocationManagerOnline implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public final static int LOG_INTERVAL = 60000;
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static GoogleApiClient mGoogleAPI;
    private static Location mLastLocationKnown;
    private static LocationRequest mRequest;
    private Context mContext;
    private boolean isSearchingLocation = false;
    private GoogleApiClient.OnConnectionFailedListener mOnConnectionFailedListener;
    private LocationListener mListener;

    public LocationManagerOnline(Context context) {

        mContext = context;
        mGoogleAPI = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mRequest = LocationRequest.create();
        mRequest = mRequest.setInterval(LOG_INTERVAL);
        mRequest = mRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mRequest = mRequest.setSmallestDisplacement(500);

        mGoogleAPI.connect();
    }

    public void setOnConnectionFailedListener(GoogleApiClient.OnConnectionFailedListener listener) {
        mOnConnectionFailedListener = listener;
    }

    public void requestLocation() {
        if (!mGoogleAPI.isConnected()) {
            isSearchingLocation = true;
            mGoogleAPI.connect();
        }
    }

    public void setListener(LocationListener listener) {
        mListener = listener;
    }

    public Location getLastLocationKown() {
        try {
            if (mLastLocationKnown == null && (mGoogleAPI != null && mGoogleAPI.isConnected())) {
                int hasFineLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
                int hasCoarseLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION);
                if (hasFineLocation == PackageManager.PERMISSION_GRANTED && hasCoarseLocation == PackageManager.PERMISSION_GRANTED) {
                    mLastLocationKnown = LocationServices.FusedLocationApi.getLastLocation(mGoogleAPI);
                }
            }
            return mLastLocationKnown;
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        isSearchingLocation = true;
        mLastLocationKnown = location;
        if (mListener != null)
            mListener.onLocationChanged(location);
    }


    @Override
    public void onConnected(Bundle bundle) {
        try {
            isSearchingLocation = true;
            if (mGoogleAPI != null && mGoogleAPI.isConnected()) {
                int hasFineLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION);
                int hasCoarseLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION);
                if (hasFineLocation != PackageManager.PERMISSION_GRANTED && hasCoarseLocation != PackageManager.PERMISSION_GRANTED)
                    return;

                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleAPI,
                        mRequest, this);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public boolean isSearchingLocation() {
        return isSearchingLocation;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        isSearchingLocation = false;
        if (mOnConnectionFailedListener != null) {
            mOnConnectionFailedListener.onConnectionFailed(connectionResult);
        }
    }
}
