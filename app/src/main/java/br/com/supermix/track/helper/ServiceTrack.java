package br.com.supermix.track.helper;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import br.com.supermix.track.domain.Route;
import br.com.supermix.track.domain.Trip;
import br.com.supermix.track.repository.RouteRepository;
import br.com.supermix.track.repository.TripRepository;

/**
 * Created by jordansilva on 2/10/16.
 */

public class ServiceTrack extends Service {

    TrackingManager mTrackingManager;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mTrackingManager = new TrackingManager(this);

        track();
        stopSelf();
        return Service.START_STICKY;
    }

    protected void track() {
        try {
            Route route = mTrackingManager.track(this);
            TripRepository tripRepository = new TripRepository(getApplicationContext());
            Trip trip = tripRepository.getActiveTrip();
            if (trip != null)
                route.trip = trip;

            RouteRepository repository = new RouteRepository(getApplicationContext());
            repository.save(route);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
