package br.com.supermix.track.helper;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Looper;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.inject.Inject;

import br.com.supermix.track.domain.Route;

/**
 * Created by jordansilva on 2/10/16.
 */
public class TrackingManager {

    Context mContext;
    @Inject
    LocationManagerOnline mLocationManagerOnline;
    @Inject
    LocationManagerOffline mLocationManagerOffline;

    public TrackingManager(Context context) {
        mContext = context;
        mLocationManagerOnline = new LocationManagerOnline(mContext);
        mLocationManagerOffline = new LocationManagerOffline(mContext);
    }

    public Route track(Service service) {

        Route route = new Route();

        try {
            Location location = getLocation();
            if (location != null) {

                route.latitude = location.getLatitude();
                route.longitude = location.getLongitude();
                route.accuracy = location.getAccuracy();
                route.provider = location.getProvider();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        route.batteryLevel = getBatteryLevel(service);
        route.date = Calendar.getInstance(TimeZone.getTimeZone("UTC"),
                Locale.getDefault()).getTime();
        return route;
    }

    private Location getLocation() {
        if (Looper.myLooper() == null)
            Looper.prepare();

        Location myLocation = mLocationManagerOnline.getLastLocationKown();
        if (myLocation == null || (myLocation.getLatitude() == 0 && myLocation.getLongitude() == 0))
            myLocation = mLocationManagerOffline.getLastLocationKown();

        return myLocation;
    }

    public float getBatteryLevel(Service service) {
        Intent batteryIntent = service.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float) level / (float) scale) * 100.0f;
    }

}


