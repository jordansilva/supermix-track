package br.com.supermix.track.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.table.TableUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.Callable;

import br.com.supermix.track.domain.DomainBase;
import br.com.supermix.track.helper.StringHelper;
import dalvik.system.DexFile;

/**
 * Created by jordansilva on 2/10/16.
 */
public class DBHelper<T extends DomainBase> extends OrmLiteSqliteOpenHelper {

    protected Context mContext;
    private static final String DATABASE_NAME = "data.sqlite";
    private static final int DATABASE_VERSION = 1;

    private Class<T> table = null;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
        if (!checkDatabase())
            copyDataBase(DATABASE_NAME);

    }

    public DBHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory,
                    int databaseVersion) {
        super(context, databaseName, null, databaseVersion);
        mContext = context;
        if (!checkDatabase())
            copyDataBase(DATABASE_NAME);
    }

    protected synchronized Dao getDao() throws SQLException {
        if (table == null)
            loadTable();

        //getWritableDatabase();
        return getDao(table);
    }

    protected void loadTable() {
        final ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
        table = (Class<T>) (type).getActualTypeArguments()[0];
    }

    public T get(Object code) throws SQLException {
        return (T) getDao().queryForId(code);
    }

    public void save(T item) throws SQLException {
        getDao().createOrUpdate(item);
    }

    public void save(final Collection<T> item) throws Exception {
        final Dao dao = getDao();

        dao.callBatchTasks(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                for (T t : item) {
                    dao.createOrUpdate(t);
                }

                return null;
            }
        });
    }

    public void insert(T item) throws SQLException {
        getDao().create(item);
    }

    public void update(T item) throws SQLException {
        getDao().update(item);
    }

    public void delete(T item) throws SQLException {
        getDao().delete(item);
    }

    public void deleteById(Object code) throws SQLException {
        getDao().deleteById(code);
    }

    public List<T> listAll() throws SQLException {
        return getDao().queryForAll();
    }

    protected QueryBuilder newQuery() throws SQLException {
        return getDao().queryBuilder();
    }

    protected List<T> List(PreparedQuery<T> query) throws SQLException {
        return getDao().query(query);
    }

    protected T Get(PreparedQuery<T> query) throws SQLException {
        return (T) getDao().queryForFirst(query);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {

        if (table == null)
            loadTable();

        createByClasses();
        executeInserts(db, connectionSource);
    }

    protected boolean executeScript(SQLiteDatabase db, ConnectionSource connectionSource, InputStream stream) {
        return executeScript(db, connectionSource, stream, false);
    }

    protected void executeInserts(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            InputStream stream = mContext.getAssets().open("data/insert.sql");
            executeScript(db, connectionSource, stream, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected boolean executeScript(SQLiteDatabase db, ConnectionSource connectionSource, InputStream stream,
                                    boolean transaction) {
        try {
            if (transaction)
                db.beginTransaction();

            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

            String line = reader.readLine();
            while (line != null) {
                if (!StringHelper.isNullOrEmpty(line))
                    db.execSQL(line);

                line = reader.readLine();
            }

            if (transaction)
                db.setTransactionSuccessful();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (transaction)
                db.endTransaction();
        }

        return false;
    }

    protected void createByClasses() {
        try {
            String packageName = DomainBase.class.getPackage().getName();

            Class[] classes = getClasses(mContext, packageName);
            for (Class cls : classes) {
                if (cls.isAnnotationPresent(DatabaseTable.class))
                    TableUtils.createTable(connectionSource, cls);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Class[] getClasses(Context mContext, String packageName) throws ClassNotFoundException, IOException {
        DexFile df = new DexFile(mContext.getPackageCodePath());
        Enumeration<String> entries = df.entries();
        ArrayList<Class> classes = new ArrayList<Class>();
        while (entries.hasMoreElements()) {
            String entry = (String) entries.nextElement();
            if (entry.contains(packageName))
                classes.add(Class.forName(entry.replace(".class", "")));
        }

        return classes.toArray(new Class[classes.size()]);
    }

    protected void refreshTables() {
        try {
            String packageName = DomainBase.class.getPackage().getName();

            Class[] classes = getClasses(mContext, packageName);
            for (Class cls : classes) {
                if (cls.isAnnotationPresent(DatabaseTable.class))
                    TableUtils.createTableIfNotExists(connectionSource, cls);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            String filename = "data/update-%s.sql";

            oldVersion++;
            while (oldVersion <= newVersion) {
                try {
                    refreshTables();

                    InputStream stream = mContext.getAssets().open(String.format(filename, oldVersion));
                    boolean result = executeScript(db, connectionSource, stream, true);

                    if (!result)
                        throw new Exception("Erro ao atualizar banco de dados!");

                    oldVersion++;

                } catch (FileNotFoundException e) {
                    Log.e(DBHelper.class.getSimpleName(), Log.getStackTraceString(e));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // super.onDowngrade(db, oldVersion, newVersion);
    }

    /**
     * Copies your database from your local assets-folder to the just created
     * empty database in the system folder, from where it can be accessed and
     * handled. This is done by transfering bytestream.
     */
    private void copyDataBase(String dbname) {
        try {

            // Open your local db as the input stream
            InputStream myInput = mContext.getAssets().open(dbname);
            // Path to the just created empty db
            File file = mContext.getDatabasePath(DATABASE_NAME);
            if (!file.exists()) {
                if (!file.getParentFile().exists())
                    file.getParentFile().mkdirs();

                file.createNewFile();
            }
            // Open the empty db as the output stream
            OutputStream myOutput = new FileOutputStream(file);
            // transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            // Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
     * Check whether or not database exist
     */
    private boolean checkDatabase() {
        return mContext.getDatabasePath(DATABASE_NAME).exists();
    }
}
