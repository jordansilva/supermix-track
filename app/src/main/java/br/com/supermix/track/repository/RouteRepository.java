package br.com.supermix.track.repository;

import android.content.Context;

import javax.inject.Inject;

import br.com.supermix.track.domain.Route;
import br.com.supermix.track.domain.Trip;

/**
 * Created by jordansilva on 2/10/16.
 */
public class RouteRepository extends DBHelper<Route> {

    @Inject
    public RouteRepository(Context context) {
        super(context);
    }
}
