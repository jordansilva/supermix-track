package br.com.supermix.track.repository;

import android.content.Context;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;

import br.com.supermix.track.domain.Trip;

/**
 * Created by jordansilva on 2/10/16.
 */
public class TripRepository extends DBHelper<Trip> {

    @Inject
    public TripRepository(Context context) {
        super(context);
    }

    public Trip getActiveTrip() throws SQLException {
        return Get(newQuery().where().isNull("endTrip").prepare());
    }

    public List<Trip> listOpenTrips() throws SQLException {
        return List(newQuery().where().isNull("endTrip").prepare());
    }

    public List<Trip> listHistoryTrips() throws SQLException {
        return List(newQuery().where().isNotNull("endTrip").prepare());
    }
}
