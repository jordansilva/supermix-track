package br.com.supermix.track.service;

import android.content.Context;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.supermix.track.domain.POI;
import br.com.supermix.track.domain.Trip;
import br.com.supermix.track.repository.TripRepository;

/**
 * Created by jordansilva on 2/7/16.
 */
@Singleton
public class PlaceService extends ServiceBase {

    @Inject
    TripRepository tripRepository;

    @Inject
    public PlaceService(Context context) {
        super(context);
    }


    public boolean simulate(UUID idWarehouse, UUID idDestination) {
        try {
            TripRepository repository = new TripRepository(mContext);
            List<Trip> trips = repository.listOpenTrips();
            if (trips != null && trips.size() > 0)
                return true;

            POI warehouse = new POI();
            warehouse.id = idWarehouse;

            POI destination = new POI();
            destination.id = idDestination;

            Trip trip = new Trip(warehouse, destination);

            Date time = Calendar.getInstance(TimeZone.getTimeZone("UTC"),
                    Locale.getDefault()).getTime();

            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"),
                    Locale.getDefault());
            calendar.setTime(time);
            calendar.add(Calendar.HOUR, 1);
            trip.estimateArrivalTime = calendar.getTime();

            calendar.add(Calendar.MINUTE, 30);
            trip.estimateStartLandTime = calendar.getTime();

            repository.save(trip);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean saveTrip(Trip mTrip) {
        try {
            TripRepository repository = new TripRepository(mContext);
            repository.save(mTrip);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ArrayList<Trip> listTrips() {
        try {
            return (ArrayList<Trip>) new TripRepository(mContext).listOpenTrips();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Trip> listHistoryTrips() {
        try {
            return (ArrayList<Trip>) new TripRepository(mContext).listHistoryTrips();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<POI> getWareHouses() {
        return new ArrayList<>();
    }

    public static ArrayList<POI> getNearestWareHouses(double latitude, double longitude, int radius) {
        return new ArrayList<>();
    }

    public static ArrayList<POI> getPOIDirections(POI location, POI destination) {
        return new ArrayList<>();
    }

}
