package br.com.supermix.track.service;

import android.content.Context;

/**
 * Created by jordansilva on 2/10/16.
 */
public class ServiceBase {

    public Context mContext;

    public ServiceBase(Context context) {
        mContext = context;
    }
}
